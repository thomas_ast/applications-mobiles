package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.view.View;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listview = (ListView) findViewById(R.id.listview);

        String[] animalList = AnimalList.getNameArray();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, animalList);

        listview.setAdapter(arrayAdapter);

        listview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               Intent animalActivity = new Intent(MainActivity.this, AnimalActivity.class);
               String name = listview.getItemAtPosition(position).toString();
               Animal animal = AnimalList.getAnimal(name);

               animalActivity.putExtra("name", name); // Nom de l'animal
               startActivity(animalActivity);
            }
        });
    }
}
