package com.example.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        final String animalName = getIntent().getStringExtra("name");
        Animal animal = AnimalList.getAnimal(animalName);

        TextView title = (TextView) findViewById(R.id.title);
        ImageView img = (ImageView) findViewById(R.id.img);
        TextView espVieMaxValue = (TextView) findViewById(R.id.espVieMaxValue);
        TextView periodeGestValue = (TextView) findViewById(R.id.periodeGestValue);
        TextView poidsNaissValue = (TextView) findViewById(R.id.poidsNaissValue);
        TextView poidsAgeAdValue = (TextView) findViewById(R.id.poidsAgeAdValue);
        final TextView statutConsValue = (TextView) findViewById(R.id.statutConsValue);
        Button btn = (Button) findViewById(R.id.btn);

        title.setText(animalName); // énoncé passage extra intent
        img.setImageResource(getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName()));
        espVieMaxValue.setText(animal.getStrHightestLifespan());
        periodeGestValue.setText(animal.getStrGestationPeriod());
        poidsNaissValue.setText(animal.getStrBirthWeight());
        poidsAgeAdValue.setText(animal.getStrAdultWeight());
        statutConsValue.setText(animal.getConservationStatus());

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnimalList.getAnimal(animalName).setConservationStatus(statutConsValue.getText().toString());
                Toast.makeText(AnimalActivity.this, "La modification a été enregistrée", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
